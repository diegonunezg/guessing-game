use rand::Rng;
use std::cmp::Ordering;
use std::io;


fn main() {
    println!("=======================================");
    println!("\t Adivina el numero!");
    println!("=======================================");

    let secret_number = rand::thread_rng().gen_range(1..=100);

    loop {
        
        println!("\nPor favor ingresa un numero: ");
        

        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Fallo al leer linea");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("Has adivinado: {guess}");

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("El numero secreto es mayor"),
            Ordering::Greater => println!("El numero secreto es menor"),
            Ordering::Equal => {
                println!("Ganaste!");
                break;
            }
        }
    }
}